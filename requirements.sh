#!/bin/bash

sudo apt-get update && sudo apt-get upgrade

sudo apt-get install -y libusb-1.0-0-dev libprotobuf-dev\
 libgdk-pixbuf2.0-dev libpango1.0-dev\
 libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5\
 libjpeg-dev libtiff5-dev libjasper-dev libpng-dev\
 libleveldb-dev libsnappy-dev\
 libfontconfig1-dev libcairo2-dev\
 libopencv-dev libhdf5-dev libhdf5-serial-dev libhdf5-103\
 protobuf-compiler libatlas-base-dev\
 git automake byacc lsb-release\
 build-essential cmake pkg-config libgflags-dev\
 libgoogle-glog-dev\
 liblmdb-dev swig3.0 graphviz\
 libxslt-dev libxml2-dev\
 libxvidcore-dev libx264-dev\
 libavcodec-dev libavformat-dev libswscale-dev libv4l-dev\
 gfortran\
 python3-dev python-pip python3-pip\
 python3-setuptools python3-markdown\
 python3-pillow python3-yaml python3-pygraphviz\
 python3-h5py python3-nose python3-lxml\
 python3-matplotlib python3-numpy\
 python3-protobuf python3-dateutil\
 python3-skimage python3-scipy\
 python3-six python3-networkx

 pip3 install picamera imutils opencv-python opencv-contrib-python==4.1.0.25