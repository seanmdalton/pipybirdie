#  python object_recognition_demo.py --prototxt models/MobileNetSSD/MobileNetSSD_deploy.prototxt.txt --model models/MobileNetSSD/MobileNetSSD_deploy.caffemodel

# import the necessary packages
from imutils.video import VideoStream #provides videostreaming support 
import imutils #general image processing/manipulation tooling
from utils.fps import FPS #custom fork of imutils.fps, allows us to keep track of video processing stats
import cv2 #computer vision module


import numpy #math stuff
import argparse #allows for taking in, and parsing command line arguments
import time #need this for the sleep function
from datetime import datetime #dates and times















from goprocam import GoProCamera
from goprocam import constants

# instantiate gopro object, and configure some photo options for the Hero 4 Black
camera = GoProCamera.GoPro(constants.gpcontrol)
camera.mode(constants.Mode.PhotoMode, constants.Mode.SubMode.Photo.Single)
camera.gpControlSet(constants.Photo.PROTUNE_PHOTO, constants.Photo.ProTune.ON)
camera.gpControlSet(constants.Photo.EVCOMP, constants.Photo.EvComp.Zero)
















# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

ap.add_argument("-p", "--prototxt", required=True, help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,    help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2,	help="minimum probability to filter weak detections")

args = vars(ap.parse_args())





















# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]

COLORS = numpy.random.uniform(0, 255, size=(len(CLASSES), 3))

#load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])





















# initialize the video stream, 
print("[INFO] starting video stream...")

# vs = VideoStream(src=0).start()   #webcam 

# vs = VideoStream(usePiCamera=True).start()     #picam

vs = VideoStream(src="udp://127.0.0.1:10000").start()     #gopro

#allow the camera sensor to warm up
time.sleep(2.0) 
#initialize the FPS counter
fps = FPS().start()





















# loop over the frames from the video stream
while True:
	# grab the frame from the threaded video stream and resize it
	# to have a maximum width of 400 pixels
	frame = vs.read()
	frame = imutils.resize(frame, width=400)
	# grab the frame dimensions and convert it to a blob
	(h, w) = frame.shape[:2]

	# blob from image does 3 things 	
    # Creates 4-dimensional blob from image. Mean subtraction - Scaling  - And optionally channel swapping
	# https://www.pyimagesearch.com/2017/11/06/deep-learning-opencvs-blobfromimage-works/
	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)
	
	# pass the blob through the network and obtain the detections and
	# predictions
	net.setInput(blob)
	detections = net.forward()






















		# loop over the detections
	for i in numpy.arange(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the prediction
		confidence = detections[0, 0, i, 2]
		# filter out weak detections by ensuring the `confidence` is
		# greater than the minimum confidence
		if confidence > args["confidence"]:
			# extract the index of the class label from the
			# `detections`, then compute the (x, y)-coordinates of
			# the bounding box for the object
			idx = int(detections[0, 0, i, 1])
			box = detections[0, 0, i, 3:7] * numpy.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")
			# draw the prediction on the frame
			label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
			
			cv2.rectangle(frame, (startX, startY), (endX, endY), COLORS[idx], 2)
			
			y = startY - 15 if startY - 15 > 15 else startY + 15

			cv2.putText(frame, label, (startX, y),	cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)














	# show the output frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF
	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break
	




















	# update the FPS counter
	fps.update()
	
	print("[INFO] elapsed time: {:.0f}".format(fps.elapsed()) + "s, approx. FPS: {:.1f}".format(fps.fps()), end="\r")






















# stop the timer and display FPS information
fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()