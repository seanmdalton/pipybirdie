#!/bin/bash 

REQ="gopro_keepalive.py"

ps_out=`ps -ef | grep $REQ | grep -v 'grep' | grep -v $0`
result=$(echo $ps_out | grep "$REQ")

args="--prototxt models/MobileNetSSD/MobileNetSSD_deploy.prototxt.txt\
 --model models/MobileNetSSD/MobileNetSSD_deploy.caffemodel\
 --out=./captures/"

if [[  $(pip show goprocam &> /dev/null) && "$result" != "" ]];then
    args="${args} --input=gopro"
    echo "[INFO] starting python detection script with gopro support"
elif pip show picamera &> /dev/null; then
    args="${args} --input=pi"
    echo "[INFO] starting python detection script with picamera support"
else
    echo $args
    echo "[ERROR] unable to determine input method"
    exit
fi


`which python` real_time_bird_detection.py $args