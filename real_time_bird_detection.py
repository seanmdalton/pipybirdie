### Code/ideas mostly taken from: 
#### https://www.pyimagesearch.com/2017/10/16/raspberry-pi-deep-learning-object-detection-with-opencv/
#### https://www.pyimagesearch.com/2017/09/18/real-time-object-detection-with-deep-learning-and-opencv/

# import the necessary packages
from imutils.video import VideoStream
from utils.fps import FPS
import numpy as np
import argparse
import imutils
import time
import cv2
import socket
import sys
from datetime import datetime
from numpy import random

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,
	help="type of camera to use ('pi' or 'gopro') ")
ap.add_argument("-p", "--prototxt", required=True,
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
	help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
	help="minimum probability to filter weak detections")
ap.add_argument("-o", "--out", default="./captures/",
	help="Location to save the cv2 images with bounding boxes and confidence")
ap.add_argument("-s", "--showwindow", default=True,
	help="Open the CV2 window (requires GUI to be running)")    
args = vars(ap.parse_args())

def take_photo ( frame, camera ):
	"This handles the generic logic to take photos for both types of cameras"

	datestring = datetime.now().strftime('%Y%m%d_%Hh%Mm%Ss%f')

	if args["input"] == "pi":
		filename = args["out"] + '/bird-pi-' + datestring + '.jpg'
		camera.start_preview()
		time.sleep(5)
		camera.capture(filename)
		camera.stop_preview()

		print("[INFO] file '" + filename + "' saved!")

	elif args["input"] == "gopro":
		camera.shutter("stop")

		# randomly choose to take a photo, or 5 second video
		if random.choice((True, False)):
			if camera.IsRecording() == 0:
				camera.shoot_video(5)	
		else:
			camera.take_photo()

		# at this point the photo/video will be saved on the actual gopro, so nothing else for this script to do

	# write the frame out to our capture location
	filename = args["out"] + '/bird-cv2-' + datestring + '.jpg'
	cv2.imwrite(filename, frame)
	print("[INFO] file '" + filename + "' saved!")

	return True;

# Function definition is here
def detect( frame, camera ):
	"This handles the generic detection logic for both types of cameras"

	# grab the frame dimensions and convert it to a blob
	(h, w) = frame.shape[:2]
	blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)

	# pass the blob through the network and obtain the detections and
	# predictions
	net.setInput(blob)

	# set the neural network target to the intel chipset 
	# net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

	detections = net.forward()

	# loop over the detections
	for i in np.arange(0, detections.shape[2]):
		# extract the confidence (i.e., probability) associated with
		# the prediction
		confidence = detections[0, 0, i, 2]

		# filter out weak detections by ensuring the `confidence` is
		# greater than the minimum confidence
		if confidence > args["confidence"]:
			# extract the index of the class label from the
			# `detections`, then compute the (x, y)-coordinates of
			# the bounding box for the object
			idx = int(detections[0, 0, i, 1])
			box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
			(startX, startY, endX, endY) = box.astype("int")

            # since we only care about birds, and I haven't bothered to retrain the model
			if CLASSES[idx] == "bird":
				print("[INFO] bird detected ("+str(datetime.now().strftime('%c'))+")! Taking photo...")

				take_photo(frame, camera)

				# to not overwhelm the system, take a pause after each capture
				time.sleep(5.0)

				# draw the prediction on the frame
				label = "{}: {:.2f}%".format(CLASSES[idx],
					confidence * 100)
				cv2.rectangle(frame, (startX, startY), (endX, endY),
					COLORS[idx], 2)
				y = startY - 15 if startY - 15 > 15 else startY + 15
				cv2.putText(frame, label, (startX, y),
					cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

		if args["showwindow"] == "true":
			# show the output frame
			cv2.imshow("Frame", frame)
			key = cv2.waitKey(1) & 0xFF

			# if the `q` key was pressed, break from the loop
			if key == ord("q"):
				break

	return

print("[INFO] initializing caffe classes and model...")
# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
"dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

if args["input"] == "pi":
	print("[INFO] initializing pi camera...")
	from picamera.array import PiRGBArray
	from picamera import PiCamera

	# initialize the camera and grab a reference to the raw camera capture
	camera = PiCamera()
	# camera.resolution = (640, 480)
	camera.framerate = 15
	rawCapture = PiRGBArray(camera)

	#allow the camera to wake up
	time.sleep(2.0)
	fps = FPS().start()

	print("[INFO] starting detection...")

	# loop over the frames from the video stream
	try:
		for frame_array in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
			# grab the frame from the threaded video stream and resize it
			# to have a maximum width of 400 pixels
			frame = frame_array.array	

			detect(frame, camera)

			# update the FPS counter
			fps.update()

			rawCapture.truncate(0)

			print("[INFO] elapsed time: {:.0f}".format(fps.elapsed()) + "s, approx. FPS: {:.1f}".format(fps.fps()), end="\r")

	except KeyboardInterrupt:
		pass

elif args["input"] == "gopro":
	print("[INFO] initializing gopro camera...")
	from goprocam import GoProCamera
	from goprocam import constants

	# instantiate gopro object, and configure some photo options for the Hero 4 Black
	camera = GoProCamera.GoPro(constants.gpcontrol)
	camera.mode(constants.Mode.PhotoMode, constants.Mode.SubMode.Photo.Single)
	camera.gpControlSet(constants.Photo.PROTUNE_PHOTO, constants.Photo.ProTune.ON)
	camera.gpControlSet(constants.Photo.EVCOMP, constants.Photo.EvComp.Zero)

	# initialize the video stream, allow the camera sensor to warm up,
	# and initialize the FPS counter
	print("[INFO] starting video stream...")
	source = "udp://127.0.0.1:10000"
	vs = VideoStream(src=source).start() # vs = VideoStream(usePiCamera=True).start()

	#allow the camera to wake up
	time.sleep(2.0)
	fps = FPS().start()

	print("[INFO] starting detection...")

	# loop over the frames from the video stream
	try:
		while True:
			frame = vs.read()
			frame = imutils.resize(frame, width=400)

			detect(frame, camera)

			# update the FPS counter
			fps.update()			

			print("[INFO] elapsed time: {:.1f}".format(fps.elapsed()) + ", approx. FPS: {:.1f}".format(fps.fps()), end="\r")

	except KeyboardInterrupt:
		vs.stop() 
		pass
else:
	print("[ERROR] No input camera type specified!")
	sys.exit()

# stop the timer and display FPS information
fps.stop()

print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()