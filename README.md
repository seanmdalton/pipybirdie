# pipybirdie

#### Real time bird detection and classification using Python on a Raspberry Pi (support for GoPro and PiCam)

aka, how easy it can be to apply ML, without knowing ML, but let’s cover some ML

## Background
```
What’s a good pandemic hobby?
I should buy a bird feeder!
I have an old GoPro around, I should mount it on the bird feeder and use the mobile app to take photos.
But then I have to sit around and wait for birds. I’m lazy. The GoPro Android app is kinda awful.
Maybe I can just make my Raspberry Pi stream the video and do simple motion detection to automate taking photos.
Wait, I can probably do one better!
... so I created a bird surveillance state in my backyard.
```
## Results
<img src="./docs/bird-20200606_14h00m23s971604.jpg" alt="drawing" width="250"/>
<img src="./docs/bird-20200606_14h01m09s335403.jpg" alt="drawing" width="250"/>
<img src="./docs/birbs1.gif" alt="drawing" width="300"/>

## Quick Start
### PiCamera
`pip3 install picamera imutils opencv-python opencv-contrib-python`  
`./start_detection.sh`  

### GoPro
`pip3 install goprocam imutils opencv-python opencv-contrib-python`  
`python gopro_keepalive.py` (in one terminal)  
`./start_detection.sh` (in a different terminal)    

## Hardware/Setup
● Raspberry Pi 4 Model B Quad Core 64 Bit, WiFi, Bluetooth, 2GB RAM  
● 5MP 1080P Video Camera Module for Raspberry Pi  
● Wireless Network Card Adapter Dongle _(optional)_  
● GoPro Hero 4 Black _(optional)_  
● Intel Neural Compute Stick 2 _(optional)_  

## Development Environment
● Python, and Python modules:  
- python-opencv (Computer Vision)  
- imutils (Image Processing)  
- picamera (Hardware support for PiCamera)  
- opencv-contrib-python (Intel DNN, OpenVINO)  
- goprocam (git/KonradIT/gopro-py-api)  

● VSCode  
- Remote SSH  
- Python (linting, debugging remote)  

## Approach
● Use Pre-Trained Models  
  
- Now: caffe/GoogleMobileNetSSD (detects and classifies 21 different types of objects, including birds)  
- Future: caffe/TinyYolo + caffe/GoogLeNet (bird recognition and classification program)  
  
## References
https://www.pyimagesearch.com/2017/10/16/raspberry-pi-deep-learning-object-detection-with-opencv/  
https://www.pyimagesearch.com/2017/09/18/real-time-object-detection-with-deep-learning-and-opencv/  
https://github.com/KonradIT/gopro-py-api  
Caffe (deep learning framework): https://caffe.berkeleyvision.org/  
Model Zoo: https://modelzoo.co/  

